import * as express from 'express';
import * as bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import { NextResolverFn } from 'graphql-tools/dist/Interfaces';
import { GraphQLOptions } from 'apollo-server-core';

import { Database } from './entities/data';
import { Context } from './graphql/context';

import { allBooks, fantasyBooks, scienceBooks } from './graphql/resolvers/book.resolver';
import { hasMembership } from './graphql/directives/has-membership';
import { typeDefs } from './graphql/typedefs';
import { CredentialFactory } from './graphql/credential-factory';
import { user_me } from './graphql/resolvers/user.resolve';

const resolvers = {
    Query: {
        all: allBooks,
        fantasyBooks: fantasyBooks,
        scienceBooks: scienceBooks,
        me: user_me
    }
};

const directiveResolvers = {
    hasMembership: hasMembership,
};

const credentialFactory = new CredentialFactory(Database);

// Put together a schema
const schema = makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers,
    allowUndefinedInResolve: false,
    directiveResolvers
});

function getAuthorizationToken(req?: express.Request) {
    const authorization_header = req.header("Authorization");
    if (authorization_header) {
        const regex_result = /Bearer (.*)/.exec(authorization_header);
        if (regex_result) {
            const token = regex_result[1];
            return token;
        }
    }

    // No token found in authorization.
    return null;
}

const optionsFunc = (req?: express.Request, res?: express.Response): GraphQLOptions | Promise<GraphQLOptions> => {
    // This function is executed each time when the route is being used.
    // So my advice is lazy loading.
    // The original idea behind context is as a place to inject database or services.
    // Of course, we create those outside.

    // Find the token.
    const token = getAuthorizationToken(req);
    const context = <Context>{
        // Insert any token of credentials/identity so that resolvers can find it.
        token: token,

        // Insert a fake database
        database: Database,

        // Only perfom look up when asked.
        credentials: () => credentialFactory.byToken(token)

    };

    return {
        schema, // Yes, we can switch schema based on the authorization.
        context,
    };
}

const app = express();

// bodyParser is needed just for POST.
app.use('/graphql', bodyParser.json(), graphqlExpress(optionsFunc));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

app.listen(3000, () => {
    console.log(`Go to http://localhost:3000/graphiql to run queries!`);
});