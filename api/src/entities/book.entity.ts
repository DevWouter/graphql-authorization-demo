import { Membership } from "./membership.enum";
import { Category } from "./category.enum";

export class BookEntity {
    title: string;
    author: string;
    requiredMembership: Membership[];
    categories: Category[]
}