import { UserEntity } from "../user.entity";

export const userWouter = <UserEntity>{
    id: 1,
    name: 'Wouter',
    apiToken: 'token-wouter',
    adult: false, // I never grow up ^_^
};

export const userJordy = <UserEntity>{
    id: 2,
    name: 'Jordy',
    apiToken: 'token-jordy',
    adult: true
};

export const allUsers = [
    userWouter,
    userJordy,
];