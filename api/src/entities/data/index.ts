import { allBooks } from "./books.data";
import { allUsers } from "./users.data";
import { BookEntity } from "../book.entity";
import { UserEntity } from "../user.entity";

export interface IDatabase {
    readonly books: BookEntity[]
    readonly users: UserEntity[]
};

export const Database = <IDatabase>{
    books: allBooks,
    users: allUsers,
};