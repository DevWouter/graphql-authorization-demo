import { BookEntity } from "../book.entity";
import { Membership } from "../membership.enum";
import { Category } from "../category.enum";

export const bookBible = <BookEntity>{
    title: "Bible",
    author: 'Unknown',
    requiredMembership: [/* Everyone */],
    categories: []
};

export const bookDune = <BookEntity>{
    title: "Dune",
    author: 'Frank Herbert',
    requiredMembership: [Membership.member],
    categories: [Category.Fantasy, Category.Science],
};

export const bookHarryPotter = <BookEntity>{
    title: "Harry Potter and the Sorcerer's stone",
    author: 'J.K. Rowling',
    requiredMembership: [Membership.member],
    categories: [Category.Fantasy],
};

export const bookJurassicPark = <BookEntity>{
    title: 'Jurassic Park',
    author: 'Michael Crichton',
    requiredMembership: [Membership.member, Membership.adult],
    categories: [Category.Science],
};

export const allBooks = [
    bookBible,
    bookDune,
    bookHarryPotter,
    bookJurassicPark,
];