export class UserEntity {
    id: number;
    /**
     * The name of the users
     */
    name: string

    /**
     * The token that needs to be present in the API.
     */
    apiToken: string

    /** 
     * Is this user an adult?
     */
    adult: boolean
}