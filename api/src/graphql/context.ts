import { IDatabase } from "../entities/data";
import { Credentials } from "./credentials";

// The context that can be used by resolvers
export interface Context {
    token?: string;
    database: IDatabase;
    credentials: () => Promise<Credentials>;
}