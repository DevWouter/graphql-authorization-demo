import { Book } from "../../models/book";
import { BookEntity } from "../../entities/book.entity";
import { Database } from "../../entities/data";
import { Category } from "../../entities/category.enum";
import { Context } from "../context";
import { Membership } from "../../entities/membership.enum";
import { Credentials } from "../credentials";

export async function allBooks(
    source,
    args,
    context: Context,
    info
): Promise<Book[]> {
    const credential = await context.credentials();
    const books = context.database.books;
    return membershipPolicy(books, credential);
}

export async function fantasyBooks(
    source,
    args,
    context: Context,
    info
): Promise<Book[]> {
    const credential = await context.credentials();
    const books = context.database.books
        .filter(book => (book.categories || []).some(cat => cat == Category.Fantasy));
    return membershipPolicy(books, credential);
}

export async function scienceBooks(
    source,
    args,
    context: Context,
    info
): Promise<Book[]> {
    const credential = await context.credentials();
    const books = context.database.books
        .filter(book => (book.categories || []).some(cat => cat == Category.Science));
    return membershipPolicy(books, credential);
}

function membershipPolicy(books: BookEntity[], credential: Credentials): BookEntity[] {
    if (credential.memberships.length == 0) {
        // No membership.
        return books.filter(x => (x.requiredMembership || []).length == 0)
    } else {
        // The user has a membership.
        return books.filter(book => {
            const requiredMemberships = (book.requiredMembership || []);
            return requiredMemberships.every(x => credential.memberships.some(y => y == x));
        });
    }
}