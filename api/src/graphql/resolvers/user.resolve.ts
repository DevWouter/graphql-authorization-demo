import { Context } from "../context";
import { Membership } from "../../entities/membership.enum";

class User {
    name: string;
    memberships: string[];
}

export async function user_me(
    source,
    args,
    context: Context,
    info
): Promise<User> {
    const credential = await context.credentials();
    const user = context.database.users.find(x => x.id == credential.userId);
    const memberships = [Membership.member];
    if (user.adult) {
        memberships.push(Membership.adult);
    }
    return <User>{
        name: user.name,
        memberships
    };
}