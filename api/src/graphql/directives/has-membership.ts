import { Context } from '../context';
import { Membership } from '../../entities/membership.enum';

/**
 * @param next The function that is called to continue resolving the graph.
 * @param source The object (if any) which is the owner/parent of what needs to be resolved
 * @param args The arguments in the graph *definition* (in this case the allowed role)
 * @param context The context object (which can contain tokens, services, database, et cetera)
 * @param info An object information of the graph and the current resolving action
 */
export async function hasMembership(
    next: () => Promise<any>,
    source,
    args: { memberships: Membership[] },
    context: Context,
    info
) {
    const credential = await context.credentials();
    const missingMemberships = args.memberships.filter(role => {
        return !credential.memberships.some(yyy => yyy == role);
    });

    if (missingMemberships.length == 0) {
        return next();
    } else {
        throw "User is missing one of the following memberships: " + missingMemberships.join(", ");
    }
}