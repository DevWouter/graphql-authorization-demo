import { UserEntity } from "../entities/user.entity";
import { Membership } from "../entities/membership.enum";
import { IDatabase } from "../entities/data";
import { Credentials } from "./credentials";

export class CredentialFactory {
    constructor(private readonly database: IDatabase) {

    }

    byToken(token: string): Promise<Credentials> {
        return new Promise((resolve, reject) => {
            const user = this.getUserByToken(token);
            const credential = this.getCredentials(user);

            // Report we found the credentials.
            resolve(credential);
        });
    }

    private getUserByToken(token: string): UserEntity {
        const possibleUsers = this.database.users.filter(x => x.apiToken == token);
        if (possibleUsers.length == null) {
            return null;
        } else {
            return possibleUsers[0];
        }
    }

    private getCredentials(user: UserEntity): Credentials {
        if (user == null) {
            return new Credentials(); // No credentials available.
        } else {
            const credential = new Credentials();
            credential.userId = user.id;
            credential.memberships.push(Membership.member);
            if (user.adult) {
                credential.memberships.push(Membership.adult);
            }
            return credential;
        }
    }
}