// The GraphQL schema in string form
export const typeDefs = `
  directive @hasMembership(memberships: [String!]) on FIELD | FIELD_DEFINITION | QUERY

  type Query { 
      # All books, can be used by anonymous
      all: [Book] 

      # All members can fetch fantasy books
      fantasyBooks: [Book] @hasMembership(memberships: ["member"])
      
      # Only member are adult allowed to fetch the adult books
      scienceBooks: [Book] @hasMembership(memberships: ["adult", "member"])

      # Returns information about the user 
      me: User @hasMembership(memberships:["member"])
  }

  type User {
      name: String!
      memberships: [String!]
  }

  type Book { 
      title: String
      
      # Only members are allowed to read who the author is
      author: String #@hasMembership(memberships: ["member"]) 
   }
`;