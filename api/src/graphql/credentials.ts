import { UserEntity } from "../entities/user.entity";
import { Membership } from "../entities/membership.enum";
import { IDatabase } from "../entities/data";

export class Credentials {
    userId?: number;
    memberships: Membership[] = [];
}
