echo ''
echo 'Wouter: all calls'
echo '---------------------------------------------------'
curl 'http://localhost:3000/graphql?' \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Authorization: Bearer token-wouter' \
--data-binary '{"query":"{\n  me{ name, memberships } \n  all { title, author }\n  fantasyBooks {title, author}\n  scienceBooks {title, author}\n  }\n","variables":null,"operationName":null}' \
-s \
| node -e \
"\
 s=process.openStdin();\
 d=[];\
 s.on('data',function(c){\
   d.push(c);\
 });\
 s.on('end',function(){\
   console.log(JSON.stringify(JSON.parse(d.join('')),null,2));\
 });\
"