# Library store example

This is an **example** to demonstrate authorization and authentication with GraphQL.

## Running the demo

In the demo we act as if we are a library and not all content is available.

To run the server execute the following commands
```sh
cd api
npm install
npm run start:watch
```

You can then run one of the scripts under `api/example`. 
Each of the shell scripts run the exact same code except the authorization is different.

- `anonymous-all-calls.sh` will be unauthenticated user so he can only look at the public section (which contains a very old book). They also have the following restrictions:
    - They can not look up who the author of a book is
    - They can not ask for fantasy books
    - They can not ask for science books
    - They can not look up who they are.
- `wouter-all-calls.sh` is an authenticated user but since he is not an adult he can only look at books that don't require an adult membership.
- `jordy-all-calls.sh` is an authenticated user and an adult so he can retrieve all books.

## Things to keep in mind

### Everything needs to be resolved
Everything in GraphQL needs to be resolved (queries, mutations, fields and directives).
Which means we can stop resolving at any point.
We use directives to wrap query and field resolvers so that we can limit access.
If the user doesn't have access, we simply won't call the wrapped resolver.

### Context creation needs to be cheap
Each time we resolve (which also include fields) we create a context in which we put the database and the credential factory.
This needs to be as cheap as possible so all calls (except looking up the token) are deferred.